#! /bin/bash
BASEDIR=$(dirname $0)
cd $BASEDIR
. git-ftp-vars.txt
mv .git-ftp-ignore-init .git-ftp-ignore
git ftp init --user $USER --passwd $PASSWORD $URL
mv .git-ftp-ignore .git-ftp-ignore-init
