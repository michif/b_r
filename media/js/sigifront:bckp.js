$(document).foundation();



$(document).ready(function() {
	
	

	onResize();
	$('.tagnavilink').on('click', function(e) {
			$('.tagnavilink').removeClass("selected");

		var string = $(this).text().replace(/ /g,'');
		$( ".arbeit" ).parent().show();
		$( ".arbeit" ).not("."+string).parent().hide();
		$(this).addClass("selected");
	})

var overlay = new Overlay();
	// trigger
	$(".trigger").on("click", function(e) {
		var currentId = $(this).attr('myModelId');
		//var sample_content = $("script.sample-content").html();
		var sample_content = $("#" + currentId).html();
		overlay.append_content(sample_content);
		setupOverlay();
		overlay.show();

	
		var mySwiper = new Swiper('.swiper-container', {
			onInit : function() {

			},

			onSlideChangeEnd : function() {
		
			},
		

		});
		
		
	
			$('.left-arrow').on('click', function(e) {
		e.preventDefault();
		mySwiper.swipePrev();

	})

	$('.right-arrow').on('click', function(e) {
		e.preventDefault();
		mySwiper.swipeNext();
	})
	
	
		reinitSwiper(mySwiper);
		
		$('.text_button').on('click', function(e) {
		e.preventDefault();
		console.log(mySwiper.activeIndex);
		
		
		var swipe_target=0;
		if(mySwiper.activeIndex!=mySwiper.slides.length - 1){
			swipe_target=mySwiper.slides.length - 1;
			$('.text_button').html("Bilder");
		}else{
			swipe_target=0;
			$('.text_button').html("Text");

		}
		mySwiper.swipeTo(swipe_target, 500 ); 
		})
		
		/*
		$('.left-arrow').on('click', function(e) {
		e.preventDefault();
		mySwiper.swipePrev();

	})

	$('.right-arrow').on('click', function(e) {
		e.preventDefault();
		mySwiper.swipeNext();
	})*/
		


		//make cover image
		$(".swiper-slide").each(function() {
			//Get Image URL
		/*	var url = $(this).attr('url');
			var me = $(this);
			var mywidth = smallrange;

			if ($(window).width() > smallrange) {
				mywidth = mediumrange;
			}

			if ($(window).width() > mediumrange) {
				mywidth = largerange;
			}

			var request_url = baseurl + "getImage";
			//get Image in Size according to Breakpoint
			$.post(request_url, {
				url : url,
				imageWidth : mywidth
			}, function(data) {
				//load image as background-coverImage
				me.css({
					"background-image" : "url(" + baseurl + "/" + data + ")",
					"-webkit-background-size" : "cover",
					"-moz-background-size" : "cover",
					"-o-background-size" : "cover",
					"background-size" : "cover",
					"background-position" : "center",
					//"background":"#00F"
				});

			});*/

			$(this).css({

				//make here a loader
				//"background-image":"url("+baseurl+"/media/img/"+loader.gif+")",
				// "-webkit-background-size": "cover",
				// "-moz-background-size": "cover",
				// "-o-background-size": "cover",
				//"background-size":"cover",
				//"background-position": "bottom",
				//"background":"#0F0"
			});

		});
		
		

		
	});

});

//OVERLAY Manager
function fixOverlay() {
//	$('.mod-overlay .overlay-content').css("width", $(window).width());
	//$('.mod-overlay .overlay-inlay').css("height", $(window).height());

	$('.mod-overlay .overlay-content').css("height", $(window).height());
	$('.mod-overlay .overlay-content').css("width", $(window).width()/100*90);


//	$('.mod-overlay .overlay-inner-wrapper').css("height", $(window).height());

			var iH=$('.overlay-content').height()-$('.overlaytitle').outerHeight();



		$('.overlay-inlay').css({
			//background:"#F0F",
			width:"90%",
			height:iH
		//padding:"20%"
			//width:"90%",
	
			
		});

	
};

function fixSwiperHeight(swiper) {
	var h = $(window).height();
	var w = $(window).width();
//		var iH=$('.overlay-content').height()/100*80;
			var iH=$('.overlay-content').height()-$('.overlaytitle').outerHeight();



	$('.swiper-container').css({
		//height : "80%",
		height : iH/100*80,
		width: "100%",

		//width: "80%",

	});
	
	
	
	
};


function fixSwiperDim() {
	var h = $(window).height();
	var w = $(window).width();
	
	var iH=$('.overlay-content').height()-$('.overlaytitle').outerHeight();

	$('.swiper-container').css({
		//height : iH,
				height : iH/100*80,

		width: "100%",
	});
	
		$('.swiper-wrapper').css({
		//height :"100%",
	});
	
		$('.swiper-slide').css({
		//height :"100%",
		//width: w,

	});
	//swiper.reInit();
	
		fixOverlay();
	fixSwiperHeight();	
};


function setupOverlay() {
	setTimeout(function() {
		fixOverlay();
		// fixOverlay();

	}, 5);
};

function reinitSwiper(swiper) {
	setTimeout(function() {
				fixSwiperDim();
		swiper.reInit();
	}, 1);

	
};

var timer;
$(window).bind('resize', function() {
	timer && clearTimeout(timer);
	timer = setTimeout(onResize, 5);
});


function fixImagecontainerSize() {
	$(".imagecontainer").height($(".imagecontainer").width());
}


function onResize() {
	fixOverlay();
	fixSwiperHeight();
	fixImagecontainerSize();
}