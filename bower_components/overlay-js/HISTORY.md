# Changelog

## 0.1.2

- Also remove the background element when the
  destroy function is executed


## 0.1.1

- Also render templates for predefined elements  
  -> And add option to disable that
  

## 0.1.0

`INITIAL_VERSION`
