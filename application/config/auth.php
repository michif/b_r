<?php defined('SYSPATH') or die('No direct access allowed.');

return array(

	'driver'       => 'ORM',
	'hash_method'  => 'sha256',
	'hash_key'     => 'Haedain4seuhaiXauph8ohmohm0cet9u',
	'lifetime'     => Date::HOUR*24,
	'session_key'  => 'auth_user',
);
