<?php defined('SYSPATH') or die('No direct script access.');


class Frontend_Container  {
    
    
    
    
    
    public static function media_overlay_swiper($model, $container="media",$imageflythumb="h200-w200-c", $imagefly="h400", $ulclass="images", $ulattributes="", $liclass="",$aclass="",$parentModelId=""){
        $r="";
        $medialinks=$model->medialink->where("container", "=", $container)->where("visible","=", 1)->order_by('position','ASC')->find_all();
        $multimedia=$medialinks->count()>1;
        if($medialinks->count()==0)return;
        
        $r.= '<ul class="'.$ulclass.'" '.$ulattributes.'>';
        $counter=0;
        foreach($medialinks as $link){
            if($link->type==$link::TYPE_IMAGE){
                //$r.='<li class="'.$liclass.'">'.HTML::anchor("imagefly/".$imagefly."/".$link->url, HTML::image("imagefly/".$imageflythumb."/".$link->url),array("rel"=>"gallery_".$model->id, "title"=>$link->title, "class"=>"$aclass")).'</li>';
               $r.='<li class="'.$liclass.'">';
                    $r.=  '<a href ="#/overlay" class="'.$aclass.' " myModelId="' . $parentModelId.'" myNumber="'.$counter.'">';
                         $r.=    '<div class="row image">';
                            $r.=  '<div class="small-12 columns imagecontainer">';
                                $r.=   '<div class="image_wrapper">';
                                    $r.= HTML::image("imagefly/".$imageflythumb."/".$link->url);
                                $r.= '</div>';
                            $r.= '</div>';
                        $r.= '</div>';
    
                  $r.='<div class="row title">';
                        $r.='<div class="large-12 columns">';
                        $r.= '<h2>'.$link->title.'</h2>';    
                        $r.= '</div>';//  title collumn
                  $r.= '</div>';// row title
    
    
             $r.= '</a>';    
              $r.='</li>';
              
              $counter++;
            }
        }
        $r.= '</ul>';
        return $r;
    
    }
    
    
    
    
    
    
    


    public static function media_fancybox($model, $container="media", $if_small="h160" , $if_big="h500"){
        $r="";
        $medialinks=$model->medialink->where("container", "=", $container)->order_by('position','ASC')->find_all();
        $multimedia=$medialinks->count()>1;
        if($medialinks->count()==0)return;
        
        
        $r.= '<div class="medias">';
        foreach($medialinks as $link){
            if($link->type==$link::TYPE_IMAGE){
                $r.= html::anchor("imagefly/".$if_big."/".$link->url , html::image("imagefly/".$if_small."/".$link->url), array("rel"=>"gallery_".$model->id, "title"=>$link->title, "class"=>"fancybox media"));
            }else if($link->type==$link::TYPE_EMBED){
                    
                // Not sure what to do here…
                
                //$r.='<div>'.$link->media_parameters.'</div>';
            }
        }       
        $r.= '</div>';
        
        return $r;
    }
    
    

    
    

    public static function media_fly($model, $container="media", $imagefly="h400"){
        $r="";
        $medialinks=$model->medialink->where("container", "=", $container)->order_by('position','ASC')->find_all();
        $multimedia=$medialinks->count()>1;
        if($medialinks->count()==0)return;
        
        $r.='<div class="mediacontainer">';
        if($multimedia){
            $r.='<div class="medianavi"></div>'; 
        }
        
        $r.= '<div class="medias '.($multimedia?'cycle':'').'">';
        foreach($medialinks as $link){
            if($link->type==$link::TYPE_IMAGE){
                $r.='<div>'.HTML::image("imagefly/".$imagefly."/".$link->url).'</div>';
            }else if($link->type==$link::TYPE_EMBED){
                $r.='<div>'.$link->media_parameters.'</div>';
            }
        }       
        $r.= '</div>';
        $r.= '<div class="caption"></div>';     
        $r.='</div>';
        
        return $r;
    }
    
    public static function media($model, $container="media", $hasnavi=true, $maxheight=400){
        $r="";
        $medialinks=$model->medialink->where("container", "=", $container)->order_by('position','ASC')->find_all();
        $multimedia=$medialinks->count()>1;
        
        if($medialinks->count()==0)return;
        
        $r.='<div class="mediacontainer">';
        if($multimedia){
            $r.='<div class="medianavi"></div>'; 
        }
        $r.= '<div class="medias '.($multimedia?'cycle':'').'"  style="height:'.$maxheight.'px ! important;">';
        
        foreach($medialinks as $link){
            if($link->type==$link::TYPE_IMAGE){
                $r.='<div>'.HTML::image("imagefly/h".$maxheight."/".$link->url).'</div>';
            }else if($link->type==$link::TYPE_EMBED){
                $r.='<div>'.$link->media_parameters.'</div>';
            }
        }
        
        $r.= '</div>';
        $r.= '<div class="caption"></div>';     
        $r.='</div>';
        
        return $r;
    }
        

    
    public static function get_file_num($model, $container){
        $medialinks=$model->medialink->where("container", "=", $container)->order_by('position','ASC')->find_all();
        return $medialinks->count();
    }
    
    public static function files($model, $container="files"){
        $r="";
        $medialinks=$model->medialink->where("container", "=", $container)->order_by('position','ASC')->find_all();
        $r.= '<div class="files">';
        foreach($medialinks as $link){
            $r.='<div class="file">';
            if($link->title==""){
                $anchortitle=$link->filename;   
            }else{
                $anchortitle=$link->title." ".nl2br($link->text);
            }
            $r.= HTML::anchor($link->url, $anchortitle, array("target"=>"_blank"));$r.="</div>";
        }
        $r.= '</div>';
        return $r;
    }
    
    public static function filelist($model, $container="files"){
        $r="";
        $medialinks=$model->medialink->where("container", "=", $container)->order_by('position','ASC')->find_all();
        
        $r.= '<ul class="files">';
        foreach($medialinks as $link){
            $r.='<li class="file">';
            if($link->title==""){
                $anchortitle=$link->filename;   
            }else{
                $anchortitle=$link->title." ".nl2br($link->text);
            }
            $r.= HTML::anchor($link->url, $anchortitle, array("target"=>"_blank"));
            $r.="</li>";
        }
        $r.= '</ul>';
        return $r;
    }
    
}