DROP TABLE contentlinks;

CREATE TABLE `contentlinks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `linkedcontent_id` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `container` varchar(255) NOT NULL,
  `var1` text,
  `var2` text,
  `var3` text,
  `var4` text,
  `var5` text,
  UNIQUE KEY `id` (`id`),
  KEY `contentlinks_ibfk_1` (`content_id`),
  KEY `contentlinks_ibfk_2` (`linkedcontent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO contentlinks VALUES("3","915","903","","kunden","","","","","");
INSERT INTO contentlinks VALUES("4","916","905","","kunden","","","","","");



DROP TABLE contentmodules;

CREATE TABLE `contentmodules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `description` text,
  `viewfolder` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

INSERT INTO contentmodules VALUES("24","Text mit Bild","Ein Text mit einem Bild, das Standardcontentelement","templates/contents/textmitbild");
INSERT INTO contentmodules VALUES("25","Collection Auszug","Damit wird ein Auszug aus einer Sammlung dargestellt nach gewissen Filter- oder Ordnungskriterien. Zb. die neuesten 3 Newsartikel aus der Newssammlung.","templates/contents/collection_ex");
INSERT INTO contentmodules VALUES("26","Kunde","Das Contentmodule f&uuml;r die Kunden, wird vorallem auf der Kundenseite verwendet","templates/contents/kunde");
INSERT INTO contentmodules VALUES("27","Arbeit","Das Contentmodul f&uuml;r die Arbeitselemente","templates/contents/arbeit");
INSERT INTO contentmodules VALUES("28","Startseite","","templates/contents/startseite");
INSERT INTO contentmodules VALUES("30","Newsartikel","Ein Artikel speziell f&uuml;r die News, das Datum ist wichtig","templates/contents/news");
INSERT INTO contentmodules VALUES("34","Tag","","templates/contents/tag");
INSERT INTO contentmodules VALUES("36","Redirector","Leitet auf eine andere URL weiter.","templates/contents/redirector");
INSERT INTO contentmodules VALUES("37","Login Box","Ein Login-Formular für das Frontend","templates/contents/loginbox");
INSERT INTO contentmodules VALUES("38","User-Info Box","Box mit Informationen über den eingeloggten Frontend-User, Logout-Link, etc.","templates/contents/userinfobox");



DROP TABLE contents;

CREATE TABLE `contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shortname` varchar(255) NOT NULL,
  `node_id` int(11) DEFAULT NULL,
  `contentmodule_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `lang` varchar(100) DEFAULT NULL,
  `container` varchar(255) NOT NULL,
  `type` varchar(200) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `lead` text,
  `text` text,
  `tags` text,
  `code` text,
  `url` text,
  `date` varchar(255) DEFAULT NULL,
  `var1` text,
  `var2` text,
  `var3` text,
  `var4` text,
  `var5` text NOT NULL,
  `var6` text NOT NULL,
  `var7` text NOT NULL,
  `var8` text NOT NULL,
  `var9` text NOT NULL,
  `var10` text NOT NULL,
  `var11` text NOT NULL,
  `var12` text NOT NULL,
  `var13` text NOT NULL,
  `var14` text NOT NULL,
  `var15` text NOT NULL,
  `vars` text NOT NULL,
  `position` int(11) DEFAULT NULL,
  `created_on` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` varchar(255) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`shortname`)
) ENGINE=InnoDB AUTO_INCREMENT=927 DEFAULT CHARSET=utf8;

INSERT INTO contents VALUES("896","","379","37","","1","de","content","content","Login (user: \'frontend_user\', Passwort im Admin-Bereich neu setzen)","","","","","","","","378","","","","","","","","","","","","","","","","10","2013-03-06 16:11:54","1","2013-03-07 09:15:35","1");
INSERT INTO contents VALUES("897","","379","37","","1","en","content","content","Login (user: \'frontend_user\', set new password in admin-panel)","","","","","","","","378","","","","","","","","","","","","","","","","0","2013-03-06 16:12:14","1","2013-03-07 09:15:56","1");
INSERT INTO contents VALUES("898","","378","24","","1","de","content","content","Willkommen im internen Bereich","","","","","","","","","","","","","","","","","","","","","","","","10","2013-03-06 16:15:55","1","2013-03-06 16:16:14","1");
INSERT INTO contents VALUES("899","","378","24","","1","en","content","content","Welcome to the restricted area","","","","","","","","","","","","","","","","","","","","","","","","0","2013-03-06 16:16:14","1","2013-03-06 16:16:25","1");
INSERT INTO contents VALUES("900","","378","38","","1","en","footer","content","","","","","","","","","379","","","","","","","","","","","","","","","","10","2013-03-06 16:16:29","1","2013-03-06 16:16:43","1");
INSERT INTO contents VALUES("901","","378","38","","1","de","footer","content","","","","","","","","","379","","","","","","","","","","","","","","","","0","2013-03-06 16:16:43","1","2013-03-06 16:16:50","1");
INSERT INTO contents VALUES("903","","363","26","","1","de","kunden","content","hans","","","","","","","","","","","","","","","","","","","","","","","","10","2014-07-08 14:35:41","1","2014-07-08 14:36:02","1");
INSERT INTO contents VALUES("905","","363","26","","1","de","kunden","content","kurt","","","","","","","","","","","","","","","","","","","","","","","","0","2014-07-08 14:36:02","1","2014-07-08 14:36:06","1");
INSERT INTO contents VALUES("915","","363","27","","1","de","arbeiten","content","Arbeit 1","","<p>Hier ein Lead</p>","<p><span>D</span></p><p><span>Wasserspiel und blühende Bäume. Der gesamte Platz wird als einheitliche Fläche ausgebildet. Bunt blühende Baumgruppen prägen die Strassenräume im Kontrast zum offenen Vogesenplatz, dem eigentlichen Bahnhofsplatz St. Johann. Skulpturale Sitz- und Liegeinseln laden im Zusammenspiel mit Wasserfontänen und einem Brunnen zum Entspannen, als Sonnenplatz oder Treffpunkt ein</span></p><p><span><br></span></p><p><span>E</span></p><p>Water play (water features) and blossoming trees. The whole square is designed as a homogeneous surface. Colorful blossoming groves dominate the streetscapes and offer a contrast  to the Vogesen square (which is truly the Bahnhof St.Johanns square). Sculptural seating elements and sunbathing islands interact with plumes of water and a fountain and invite for people to mingle or simply relax.&nbsp;</p>","","","media/upload/hydrant-139-zuerich-17-04-2009-f508a731-0dfd-474e-b567-e20641c7f3bd-1.jpg","2014-07-02","Direktauftrag","Christoph Merian Stiftung","Fertiggestellt 2009","","","","","","","","","","","","","","10","2014-07-09 15:32:08","1","2014-08-12 17:56:36","1");
INSERT INTO contents VALUES("916","","363","27","","1","de","arbeiten","content","Arbeit 2","","<p>sadf</p>","<p><span>D</span></p><p><span>Wasserspiel und blühende Bäume. Der gesamte Platz wird als einheitliche Fläche ausgebildet. Bunt blühende Baumgruppen prägen die Strassenräume im Kontrast zum offenen Vogesenplatz, dem eigentlichen Bahnhofsplatz St. Johann. Skulpturale Sitz- und Liegeinseln laden im Zusammenspiel mit Wasserfontänen und einem Brunnen zum Entspannen, als Sonnenplatz oder Treffpunkt ein</span></p><p><span>E</span></p><p>Water play (water features) and blossoming trees. The whole square is designed as a homogeneous surface. Colorful blossoming groves dominate the streetscapes and offer a contrast to the Vogesen square (which is truly the Bahnhof St.Johanns square). Sculptural seating elements and sunbathing islands interact with plumes of water and a fountain and invite for people to mingle or simply relax.&nbsp;</p>","","","media/upload/hydrant-139-zuerich-17-04-2009-f508a731-0dfd-474e-b567-e20641c7f3bd-1.jpg","2014-07-08","Wettbewerb","ACME Garden","Fertiggestellt 20011","","","","","","","","","","","","","","0","2014-07-09 15:32:08","1","2014-08-12 17:56:36","1");
INSERT INTO contents VALUES("918","","380","30","","1","de","News","content","1 Preis noch ein Wettbewerb","","","<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>","","","","2014-07-09","","","","","","","","","","","","","","","","","10","2014-07-11 09:22:11","1","2014-07-16 10:12:01","1");
INSERT INTO contents VALUES("919","","380","30","","1","de","News","content","jaja noch mehr news","","","<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>","","","","2014-07-05","","","","","","","","","","","","","","","","","0","2014-07-11 09:22:11","1","2014-07-16 10:12:07","1");
INSERT INTO contents VALUES("923","","358","25","","1","de","content","content","Wettbewerbe","","","","","","","","363","arbeiten","&#916;","#15edd4","","","","","","","","","","","","","0","2014-07-11 09:56:10","1","2014-07-16 09:31:22","1");
INSERT INTO contents VALUES("924","","381","25","","1","de","content","content","Finito","","","","","","","","363","arbeiten","&#8747;","#e827d1","","","","","","","","","","","","","0","2014-07-16 08:56:03","1","2014-07-16 18:46:22","1");
INSERT INTO contents VALUES("925","","382","25","","1","de","content","content","Baustelle","","","","","","","","363","arbeiten","&#170;","#75e3ff","","","","","","","","","","","","","0","2014-07-16 08:56:23","1","2014-08-12 17:56:06","1");



DROP TABLE medialinks;

CREATE TABLE `medialinks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(11) unsigned NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `container` varchar(255) NOT NULL,
  `url` varchar(512) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `mimetype` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `extension` varchar(255) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `text` text,
  `alttag` varchar(255) NOT NULL,
  `media_parameters` text,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`,`content_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

INSERT INTO medialinks VALUES("2","915","1","media","/media/upload/Projekt%2001/DSC_6223.jpg","DSC_6223.jpg","image/jpeg","image","jpg","","","","","70");
INSERT INTO medialinks VALUES("3","915","1","media","/media/upload/Projekt%2001/DSC_6235.jpg","DSC_6235.jpg","image/jpeg","image","jpg","","","","","60");
INSERT INTO medialinks VALUES("4","915","1","media","/media/upload/Projekt%2001/DSC_6237.jpg","DSC_6237.jpg","image/jpeg","image","jpg","","","","","50");
INSERT INTO medialinks VALUES("5","915","1","media","/media/upload/Projekt%2001/DSC_6238.jpg","DSC_6238.jpg","image/jpeg","image","jpg","","","","","40");
INSERT INTO medialinks VALUES("6","915","1","media","/media/upload/Projekt%2001/DSC_6243.jpg","DSC_6243.jpg","image/jpeg","image","jpg","","","","","30");
INSERT INTO medialinks VALUES("7","915","1","media","/media/upload/Projekt%2001/DSC_6259.jpg","DSC_6259.jpg","image/jpeg","image","jpg","caption 3","","","","20");
INSERT INTO medialinks VALUES("8","915","1","media","/media/upload/Projekt%2001/DSC_6946.jpg","DSC_6946.jpg","image/jpeg","image","jpg","caption2","","","","10");
INSERT INTO medialinks VALUES("9","915","1","media","/media/upload/Projekt%2001/DSC_6955.jpg","DSC_6955.jpg","image/jpeg","image","jpg","caption","","","","0");
INSERT INTO medialinks VALUES("10","916","1","media","/media/upload/Projekt%2001/DSC_6946.jpg","DSC_6946.jpg","image/jpeg","image","jpg","jaja, caption","","","","10");
INSERT INTO medialinks VALUES("12","916","1","media","/media/upload/Projekt%2001/DSC_6246.jpg","DSC_6246.jpg","image/jpeg","image","jpg","titel 1","","","","0");



DROP TABLE nodemetaobjects;

CREATE TABLE `nodemetaobjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `node_id` int(11) DEFAULT NULL,
  `lang` varchar(200) DEFAULT NULL,
  `menutitle` varchar(200) DEFAULT NULL,
  `windowtitle` varchar(200) DEFAULT NULL,
  `description` text NOT NULL,
  `keywords` text NOT NULL,
  `var1` text NOT NULL,
  `var2` text NOT NULL,
  `var3` text NOT NULL,
  `var4` text NOT NULL,
  `var5` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=268 DEFAULT CHARSET=utf8;

INSERT INTO nodemetaobjects VALUES("185","340","de","Home","Window title","description","some words, go here, yes.","","","","","");
INSERT INTO nodemetaobjects VALUES("194","340","en","Home (en)","Home (en)","","","","","","","");
INSERT INTO nodemetaobjects VALUES("195","262","en","Main Menu","Main Menu","","","","","","","");
INSERT INTO nodemetaobjects VALUES("196","262","de","Hauptmenü","Hauptmenü","","","","","","","");
INSERT INTO nodemetaobjects VALUES("204","351","de","Footermenü","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("208","351","en","Footer Menu","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("213","355","de","Disclaimer","page_355","","","","","","","");
INSERT INTO nodemetaobjects VALUES("214","356","de","Impressum","page_356","","","","","","","");
INSERT INTO nodemetaobjects VALUES("215","357","de","Kontakt","Kontakt","","","","","","","");
INSERT INTO nodemetaobjects VALUES("217","357","en","Contact","page_357","","","","","","","");
INSERT INTO nodemetaobjects VALUES("218","356","en","Impressum","page_356","","","","","","","");
INSERT INTO nodemetaobjects VALUES("219","355","en","Disclaimer","page_355","","","","","","","");
INSERT INTO nodemetaobjects VALUES("220","358","de","Wettbewerbe","page_358","","","","","","","");
INSERT INTO nodemetaobjects VALUES("222","358","en","Work","Work","","","","","","","");
INSERT INTO nodemetaobjects VALUES("229","363","de","Collection","collection_363","","","","","","","");
INSERT INTO nodemetaobjects VALUES("230","363","en","Collection","Collection_Tables","","","","","","","");
INSERT INTO nodemetaobjects VALUES("244","375","de","Home","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("249","375","en","Home","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("257","378","en","Restricted Area","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("258","378","de","Interner Bereich","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("259","379","de","Login","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("260","379","en","Login","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("263","","en","","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("264","","de","","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("265","380","de","News","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("266","381","de","Finito","","","","","","","","");
INSERT INTO nodemetaobjects VALUES("267","382","de","Baustelle","","","","","","","","");



DROP TABLE nodes;

CREATE TABLE `nodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  `lvl` int(11) NOT NULL,
  `scope` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `nodetemplate_id` int(11) DEFAULT NULL,
  `shortname` varchar(255) NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(255) DEFAULT NULL,
  `displayhook` varchar(255) DEFAULT NULL,
  `title` text,
  `loginpage` text,
  `created_on` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` varchar(255) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`shortname`)
) ENGINE=InnoDB AUTO_INCREMENT=383 DEFAULT CHARSET=utf8;

INSERT INTO nodes VALUES("262","0","1","20","1","1","0","4","_262","1","navitree","mainnavi","Hauptmenü","","0000-00-00 00:00:00","0","2014-07-08 13:47:10","1");
INSERT INTO nodes VALUES("340","262","6","7","2","1","0","1","home","1","page","startpage","Homepage","","0000-00-00 00:00:00","0","2013-03-06 16:21:03","1");
INSERT INTO nodes VALUES("351","0","1","8","1","3","10","4","_351","1","navitree","footernavi","footernavi","","0000-00-00 00:00:00","0","2014-07-08 13:47:10","1");
INSERT INTO nodes VALUES("355","351","4","5","2","3","0","1","disclaimer","1","page","","page_355","","","0","2012-09-13 15:33:30","0");
INSERT INTO nodes VALUES("356","351","2","3","2","3","0","1","Impressum","1","page","","page_356","","","0","2012-09-22 14:38:18","1");
INSERT INTO nodes VALUES("357","262","10","11","2","1","0","1","kontakt","0","page","","page_357","","","0","2014-07-14 16:43:48","1");
INSERT INTO nodes VALUES("358","262","8","9","2","1","0","17","wettbewerbe","1","page","work","page_358","","","0","2014-07-16 09:26:32","1");
INSERT INTO nodes VALUES("363","0","1","2","1","5","20","14","collection363","1","collection","","Collection","","","0","2014-07-08 13:47:10","1");
INSERT INTO nodes VALUES("375","351","6","7","2","3","0","15","home2","1","page","","page_375","","","0","2012-09-22 19:11:56","1");
INSERT INTO nodes VALUES("378","262","12","13","2","1","0","1","intern","0","page","","page_378","379","2013-03-06 16:09:20","","2014-07-14 16:43:50","1");
INSERT INTO nodes VALUES("379","262","14","15","2","1","0","1","intern_login","0","page","","page_379","","2013-03-06 16:10:08","","2013-03-06 16:21:03","1");
INSERT INTO nodes VALUES("380","","1","2","1","6","30","16","collection380","1","collection","","News","","2014-07-08 13:46:56","","2014-07-08 13:47:16","1");
INSERT INTO nodes VALUES("381","262","4","5","2","1","0","1","page381","1","page","","page_381","","2014-07-16 08:55:20","","2014-07-16 08:55:20","1");
INSERT INTO nodes VALUES("382","262","2","3","2","1","0","1","page382","1","page","","page_382","","2014-07-16 08:55:21","","2014-07-16 08:55:21","1");



DROP TABLE nodetemplates;

CREATE TABLE `nodetemplates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `description` text,
  `type` varchar(255) NOT NULL,
  `containers` text NOT NULL,
  `viewfolder` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

INSERT INTO nodetemplates VALUES("1","Eine Standardseite","Das Standardtemplate, welches normalerweise angewendet werden sollte.","page","content","templates/pages/standard");
INSERT INTO nodetemplates VALUES("2","Collection Arbeiten /  Kunden","Das Template zur Darstellung von Inhalten, die direkt aus einer Collection gewonnen werden.","collection","arbeiten,kunden,tags","templates/collections/standard");
INSERT INTO nodetemplates VALUES("4","Navigationsbaum","Der Normale Navigationsbaum","navitree","","");
INSERT INTO nodetemplates VALUES("14","Collection DataTables","","collection","arbeiten,kunden,tags","templates/collections/tables");
INSERT INTO nodetemplates VALUES("15","Weiterleitung","<p>Pages mit diesem Template haben keinen Inhalt, sondern leiten einfach auf eine andere Page weiter.</p>","page","redirect","templates/pages/redirect");
INSERT INTO nodetemplates VALUES("16","News","","collection","News,Tags","templates/collections/tables");
INSERT INTO nodetemplates VALUES("17","Galerie","Page für die Galerieansicht","page","content","templates/pages/gallery");



DROP TABLE nodetemplates_contentmodules;

CREATE TABLE `nodetemplates_contentmodules` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nodetemplate_id` int(11) NOT NULL,
  `contentmodule_id` int(11) NOT NULL,
  `container` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_nodetemplate_id` (`nodetemplate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=732 DEFAULT CHARSET=utf8;

INSERT INTO nodetemplates_contentmodules VALUES("661","2","27","arbeiten");
INSERT INTO nodetemplates_contentmodules VALUES("662","2","26","kunden");
INSERT INTO nodetemplates_contentmodules VALUES("665","2","34","tags");
INSERT INTO nodetemplates_contentmodules VALUES("670","15","36","redirect");
INSERT INTO nodetemplates_contentmodules VALUES("671","14","26","kunden");
INSERT INTO nodetemplates_contentmodules VALUES("672","14","34","tags");
INSERT INTO nodetemplates_contentmodules VALUES("674","14","27","arbeiten");
INSERT INTO nodetemplates_contentmodules VALUES("725","1","24","content");
INSERT INTO nodetemplates_contentmodules VALUES("726","1","25","content");
INSERT INTO nodetemplates_contentmodules VALUES("729","17","24","content");
INSERT INTO nodetemplates_contentmodules VALUES("730","17","25","content");
INSERT INTO nodetemplates_contentmodules VALUES("731","16","30","News");



DROP TABLE roles;

CREATE TABLE `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO roles VALUES("1","login","Darf sich einloggen");
INSERT INTO roles VALUES("2","admin","Hauptadministrator, darf alles");
INSERT INTO roles VALUES("3","contentadmin","Kann Contents bearbeiten, aber nicht die Struktur der Seite verÃƒÂ¤ndern");
INSERT INTO roles VALUES("4","frontend-login","Beispiel-Rolle für den Frontend-Login");
INSERT INTO roles VALUES("5","backend-login","Rolle für User, die sich im Backend anmelden dürfen. (ausser User \'admin\', dieser darf sich sowieso im Backend anmelden)");



DROP TABLE roles_nodes;

CREATE TABLE `roles_nodes` (
  `node_id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO roles_nodes VALUES("378","4");
INSERT INTO roles_nodes VALUES("378","4");
INSERT INTO roles_nodes VALUES("378","4");



DROP TABLE roles_users;

CREATE TABLE `roles_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`),
  CONSTRAINT `roles_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `roles_users_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO roles_users VALUES("1","1");
INSERT INTO roles_users VALUES("2","1");
INSERT INTO roles_users VALUES("3","1");
INSERT INTO roles_users VALUES("1","2");
INSERT INTO roles_users VALUES("2","3");
INSERT INTO roles_users VALUES("3","4");



DROP TABLE user_tokens;

CREATE TABLE `user_tokens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(40) NOT NULL,
  `type` varchar(100) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_token` (`token`),
  KEY `fk_user_id` (`user_id`),
  KEY `expires` (`expires`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO user_tokens VALUES("2","1","d908dfafbf0541f38a44ac4c094507ce5c0bf4a6","179abb706eb5db7d18cd258fd12a7f9e41fc50db","","1405063326","1405149726");
INSERT INTO user_tokens VALUES("3","1","f1c001ad77c79e6b0d5a7408cf99c929a541ff50","55be3023c5e6b992c63ee0f7cb7bde5e6c2e6d94","","1405411817","1405498217");
INSERT INTO user_tokens VALUES("4","1","f1c001ad77c79e6b0d5a7408cf99c929a541ff50","212db60e8d64beeaa333f7d01c009dcf78a443d2","","1407858910","1407945310");



DROP TABLE users;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(254) NOT NULL,
  `username` varchar(32) NOT NULL DEFAULT '',
  `password` varchar(64) NOT NULL,
  `hasglobalrights` tinyint(1) NOT NULL,
  `logins` int(10) unsigned NOT NULL DEFAULT '0',
  `last_login` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_username` (`username`),
  UNIQUE KEY `uniq_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO users VALUES("1","contact@michaelflueckiger.ch","admin","6280dc9ef00fc1ca9f6f35c5e3de1b881efbf1bebbd36b6760f0e4299c1ed2e6","1","266","1407858910");
INSERT INTO users VALUES("2","b.rufer@robandrose.ch","client","b25464e5ce6acea5471c3e03b8a2409d3167e950507dc6a80f7c993cad23cbdc","1","12","1329050850");
INSERT INTO users VALUES("3","the_password_is@user123456.com","frontend_user","c1db7a1266642814f2f8e2515440ed604e3ad9d94ba1c082082daa9d4c2396dc","0","13","1362644214");



DROP TABLE users_nodes;

CREATE TABLE `users_nodes` (
  `user_id` int(11) NOT NULL,
  `node_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`node_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




