<?
$head = new View("templates/pages/elements/head");
$head -> description = $description;
$head -> keywords = $keywords;
$head -> title = $title;
$head -> generator = $generator;
echo $head;
?>

</head>
<body>
	
	
	
	
	<?= isset($preview) ? $preview : ''; ?>	
	
	
	
<div id="maincontainer">
<?= isset($preview)?$preview:'';?>

	<div id='langnavi'>
		<?= isset($langnavi)?$langnavi:'';?>
	</div>
	
	<div id='navi'>
	   	<div class='mainnavi'> 
		<?= isset($mainnavi)?$mainnavi:'';?> 
	    </div>
	</div>
	 
	<div id='content'>
	   	<?= isset($content)?$content:'';?>
	</div>
	
	<!--<div id='footer'>
		<?= isset($footer)?$footer:'' ?>
		<?= isset($footernavi)?$footernavi:''?>
	</div>
-->
</div>
</body>
</html>