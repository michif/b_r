<?php 
$meta= $page->get_meta_object(Session::instance()->get('frontend_language'));

if ($meta->var2!="") {
	$targetText = $meta->var2;
	header( 'Location: ' . $meta->var2 ) ;
	die();	
} else if ($meta->var1!="") {
	$targetText = $meta->var1;
	$linkedPage = ORM::factory('Node', $meta->var1);
	header( 'Location: ' . URL::base() . $linkedPage->shortname ) ;
	die();	
} else {	
	header( 'Location: ' . URL::base() . "") ;
}
?>
