<div class="page action_<?=$model->id?> <?=($model->visible?'':'invisible')?> " ">

<div class='pagehead'>
  <div class="header togglehead actionbox_container">
   
    <?=Backend_Menu::get_node_path($model)?>
    <?=Backend_Actionbox::mini('node', $model, array("delete", "visible"))?>
  
  </div>
  <div class="nodemetaobject togglebody">
    <?
  	$metaobject=$model->get_meta_object();
  	
	echo Backend_Editable::input("nodemetaobject", $metaobject, "windowtitle", "Fenstertitel");
	echo Backend_Editable::input("nodemetaobject", $metaobject, "description", "Beschreibung");
	echo Backend_Editable::input("nodemetaobject", $metaobject, "keywords", "Keywords");
	
	
	?>
   </div>
</div>

<?	

	// prepare the pagelist-dropdown
	$pagelist = Backend_Content::nested_page_tree();
	$pagelist  +=  array("selected"=>$metaobject->var1);
	
	unset($pagelist[$model->id]);	// remove myself from the list, to prevent endles redirect-loops
	echo Backend_Editable::select("nodemetaobject", $metaobject, "var1", "Interne Weiterleitung", "", NULL, NULL, $pagelist);
	echo Backend_Editable::input("nodemetaobject", $metaobject, "var2", "Externe Weiterleitung url mit http://");
	echo HTML::anchor("admin/node/redirectall/".$model->id, "redirect all languages to same spot as de version");
	
	
?>
