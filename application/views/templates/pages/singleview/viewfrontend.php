<?
$head = new View("templates/pages/elements/head");
$head -> description = $description;
$head -> keywords = $keywords;
$head -> title = $title;
$head -> generator = $generator;
echo $head;
?>

</head>
<body>
	
	
	
	
	<?= isset($preview) ? $preview : ''; ?>	
	
	
	
	
	
	
	

    <div class="sticky">
        <div class ="top-bar" data-topbar id="topbar">
            <div class ="row fullWidth">
                <div class="medium-6 small-12 columns ">
                    <a href="/"><img class="logo" src="<?echo URL::base();?>media/img/bundrlogo.jpg" alt="Logo"></a>
                </div>

                <div class="medium-6  small-12 columns">
                    <div id='navi'>
                        <div class ='mainnavi'>
                        <?= isset($mainnavi)?$mainnavi:'';?>
                        </div>
                    </div>
                </div>

            </div>
        </div><!--topbar-->
    </div>

	
    <div class ="row fullWidth" id="header">
        <div class="small-12 columns">
            <div class ="row"></div>
	   </div>
    </div><!--header-->
		
		
    <div class ="row fullWidth" id="main_content">
	   <?= isset($content)?$content:'';?>
    </div>
		
	    <div id='footer' class='row fullWidth''>
        <?= isset($footer)?$footer:'' ?>
    </div>	
		
<?

$foot = new View("templates/pages/elements/foot");
echo $foot;

echo HTML::script('media/js/jqueryBBQ.js') . PHP_EOL;


echo HTML::script('bower_components/overlay-js/overlay.js').PHP_EOL;
echo HTML::style('bower_components/overlay-js/overlay.css', array("media"=>"all")).PHP_EOL;

echo HTML::script('bower_components/swiper/dist/idangerous.swiper.min.js').PHP_EOL;
echo HTML::style('bower_components/swiper/dist/idangerous.swiper.css', array("media"=>"all")).PHP_EOL;




echo HTML::script('media/js/sigifront.js') . PHP_EOL;

echo new View("templates/pages/elements/tracking");
?>

</body>
</html>