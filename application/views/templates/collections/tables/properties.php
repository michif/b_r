<?php

// TODO: translate to english
// Diese Properties werden für die Definition der Tabellen je Container benötigt
// Dieses File kann überschrieben werden um die Tabellendefinitionen anzupassen

return array(
        "arbeiten"  =>  array(
                                     "rowReordering"=>true,
        
            "aoColumns"     =>  array(
                                    array("mData" => "position"),
                                    array("mData" => "id", "sContentPadding"=> "0"),
                                    array("mData" => "title"),
                                    array("mData" => "subtitle"),
                                    array("mData" => "date"),
                                    //array("mData" => "url"),
                                    //array("mData" => null)
                                ),
            "aoColumnDefs"  =>  array(
                                    array("sTitle" => "POS",         "aTargets"=>array(0),"bSortable"=>false,),
                                    array("sTitle" => "ID",         "aTargets"=>array(1),"bSortable"=>false,),
                                    array("sTitle" => "Titel",      "aTargets"=>array(2),"bSortable"=>false,),
                                    array("sTitle" => "subtitle",   "aTargets"=>array(3),"bSortable"=>false,),
                                    array("sTitle" => "date",       "aTargets"=>array(4),"bSortable"=>false,),
                                //  array("sTitle" => "img",        "aTargets"=>array(5),"bSortable"=>false, "mRender"=>"function(data, type, row ){
                                    //  if(data==undefined)return \"(kein Bild)\";
                                    //  return '<img src=\"".url::base()."/imageFly/h15/'+data+'\" />';}", "bSortable"=>false),                                 
                                //  array("sTitle" => "action",     "aTargets"=>array(6), "bSortable"=>false, "bSearchable"=>false)
                                    
                                )
        ),
        
        "News"  =>  array(
            "aoColumns"     =>  array(
                                    array("mData" => "id", "sContentPadding"=> "0"),
                                    array("mData" => "title"),
                                    array("mData" => "subtitle"),
                                    array("mData" => "date"),
                                    array("mData" => "url"),
                                    array("mData" => null)
                                ),
            "aoColumnDefs"  =>  array(
                                    array("sTitle" => "ID",         "aTargets"=>array(0)),
                                    array("sTitle" => "Titel",      "aTargets"=>array(1)),
                                    array("sTitle" => "subtitle",   "aTargets"=>array(2)),
                                    array("sTitle" => "date",       "aTargets"=>array(3)),
                                    array("sTitle" => "img",        "aTargets"=>array(4), "mRender"=>"function(data, type, row ){
                                        if(data==undefined)return \"(kein Bild)\";
                                        return '<img src=\"".url::base()."/imagefly/h15/'+data+'\" />';}", "bSortable"=>false),                                 
                                    array("sTitle" => "action",     "aTargets"=>array(5), "bSortable"=>false, "bSearchable"=>false)
                                    
                                )
        ),

        "kunden"    =>  array(
            "aoColumns"     =>  array(
                                    array("mData" => "id"),
                                    array("mData" => "title"),
                                ),
            "aoColumnDefs"  =>  array(
                                    array("sTitle" => "ID",     "aTargets" => array(0)),
                                    array("sTitle" => "Titel",  "aTargets" => array(1)),
                                )
        ),


        "tags"=>array(
           "rowReordering"=>true,
            "aoColumns"     =>  array(
                                    array("mData"   => "position"),
                                    array("mData"   => "title"),
                                ),
            "aoColumnDefs"  =>  array(
                                    array("sTitle"  => "ID",    "aTargets"=>array(0)),
                                    array("sTitle"  => "Titel", "aTargets"=>array(1)),
                                )
        )
);