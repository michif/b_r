<?php 
/**
 * $model is a Model_Node
 * $language is the the requested content-language. 
 * $tags is the mapping for model-properties <-> xml-tags
 * 
 * this is the default view, if a contentelement doesn't
 * have it's own viewxml.
 * 
 */

$content_attributes = array(	$tags["id"]				=> Frontend_XML::entities($model->id),
								$tags["visible"]		=> Frontend_XML::entities($model->visible),
								$tags["created_on"]		=> Frontend_XML::entities($model->created_on),
								$tags["modified_on"]	=> Frontend_XML::entities($model->modified_on),
								$tags["created_by"]		=> Frontend_XML::entities($model->created_by),
								$tags["modified_by"]	=> Frontend_XML::entities($model->modified_by)	);

$out  = Backend_Dom::wrap($tags['title'], 		Frontend_XML::entities($model->title));
$out .= Backend_Dom::wrap($tags['subtitle'],	Frontend_XML::entities($model->subtitle));
$out .= Backend_Dom::wrap($tags['lead'], 		Frontend_XML::entities($model->lead));
$out .= Backend_Dom::wrap($tags['text'], 		Frontend_XML::entities($model->text));
$out .= Backend_Dom::wrap($tags['date'], 		Frontend_XML::entities($model->date));


// CONTENTLINKS INCOMING / OUTGOING
$out .= Frontend_XML::contentlinks($model->contentlinks->find_all(), $model->contentlinksonme->find_all(), $tags);

// MEDIA
$medialinks = $model->medialink->order_by('position', 'asc')->find_all();
$out .= Frontend_XML::medialinks($medialinks, $tags);

// wrap everything up and send it back
echo Backend_Dom::wrap($tags['content'], $out, $content_attributes);